# Visualization Project

Welcome to the Visualization Project! The goal of this software is to take 3D meshes and raw data from computational powerhouses such as Mathematica and Maxima, and convert these meshes into beautiful, textured 3D shapes using computer graphics software such as Blender. The current version as of September 2020 supports Maxima and Blender communication via Python scripts.

DOCUMENTATION IS WORK IN PROGRESS.

## Getting Started
### Pre-requisites
This software was built using Python, Blender 2.8, and Maxima. 

One Blender plugin is required: 
- AnimAll

MacOS/Darwin is suggested -- Python compatibility with Windows CLI is shaky.

## The Directory
### Data directory
- **SceneBuilder** is the primary directory that contains the GUI and the main .blend files that render any specified mesh. Run the following code through a CLI: `python3 Main.py` to boot-up the GUI. 
    - **building** contains Maxima code to create the mesh as well as the .blend files that Main.py communicates with. MySurfaceTest.blend is currently the primary blender file in use.
    - **features** Main.py has several features described in the **Usage** section. This directory contains the .py files to run the features.

Code below is for development purposes:

- **common** Stores Python code to communicate with Blender API. Avoid modifying.
- **frenet** 2D curves imported into Blender as a tube mesh. Contains code for Frenet-Serret frame that is animated and slides smoothly across continuous curves.
- **surfaces** 3D surfaces. Functionality to use Maxima to import meshes.
- **ODE1** Currently non-functional. Examining the ability for Python to calculate differential fields at a quick pace along with the corresponding mesh lines and textures to send to Blender to render.
  
### Other resources

- **DESIGN.md**: Contains detailed information about each of the functions and the overall setup of the entire application!

## Usage

Run the following code through a CLI: `cd peschke/SceneBuilder`, then `python3 Main.py` to boot-up the GUI. 

![image info](./GUI-start.png)

There are 4 drawing methods, and 3 modification features, However, there are a few concepts to introduce before using any features.

The flow of creating a mesh through the GUI is as follows: 

0. Ensure the Maxima Path and Blender Path is set to the location of your local Maxima and Blender respectively. The code will attempt to search for these paths automatically, but it does not always work on all systems. 

1. Create data from the 6 leftmost entries. The three on the left is for a static mesh, and the three on the right is for animations. For simplicity, fill in a function parameterized in two variables. For example, a sphere is canonically paramterized as: 

`x(u,v) = sin(u)*sin(v)
y(u,v) = sin(u)*cos(v)
z(u,v) = cos(u)
`

2. Specify the id (name). There are additional options for specifying the parameters for a colouring of the given mesh, for restricting domain of the function, and for displaying coordinate lines. It suffices to leave them blank

3. Press Create_Data. This sends the given attributes to Maxima. Maxima will automatically run in the background and compute the desired mesh data for Blender.

4. Once the data has been made, press "Surface" or "Curve" (depending on which mesh you have created), and it will create the corresponding object in MySurfaceTest.blend. It will automatically render for you.

5. At the bottom, there is a list of all the objects that are currently in the given blender file. You can modify them according to the options given.


### DRAWING: 

Create_Data: *** ALWAYS create_data before trying to draw a curve or surface!!! *** 

**Drawing**

- Curve: in the first 3 entries, type in a curve paramaterized as (x(t),y(t),z(t)). Supply a name and a colour_id 
- Surface: similar, surface is paramterized as (x(u,v),y(u,v),z(u,v)). 
- Animate Surface: Performs a linear transformation from the first set of surface (f) and the second set surface (g) in t steps. i.e. k*f(u,v) + (1-k)*g(u,v) where k = 1/(t-1) 
- Render: Uses blender rendering engine 'EEVEE' to render the current scene. Currently no animation rendering has been implemented. To view animations and/or models; open building/MySurfaceTest.blend in blender. 

**Modification**

- Colours: Using the 3 entries on the rightmost side, define a colour function for each point uv in the domain. 
- Delete Object: delete object from memory (invisible). Does not delete mesh. 
- Modify Material: Assigns a material to the selected object. Materials are either pre-generated or user-generated. 

---- Please let me know if anything wrong shows up!

## Video
A video will be uploaded to YouTube soon!


