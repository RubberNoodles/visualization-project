from math import sqrt, cos, sin, pi
class Data(): #Coordinate Lines v=0
    def __init__(self,x,y,z):
        self.locx = -x*(1/(1+self.Norm(x,y,0)))-y
        self.locy = -y*(1/(1+self.Norm(x,y,0)))+x
        self.locz = 0
    def Norm(self,x,y,z):
        return sqrt(x**2+y**2+z**2)
