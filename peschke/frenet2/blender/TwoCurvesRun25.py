from math import floor, ceil
#import Blender
#from Blender import Mesh, Object, Material, Mathutils, Modifier, Scene
#from Blender import Ipo, IpoCurve
import bpy, mathutils, imp

import TwoCurves
imp.reload(TwoCurves)
import BlenderCurves25
imp.reload(BlenderCurves25)

def run():
    curve1 = TwoCurves.MyCurve1()
    curve2 = TwoCurves.MyCurve2()

    try:
        vizparms = TwoCurves.VizParms()
    except:
        vizparms = None

    SetAnimParms(vizparms)
    ob1 = MakeCurve("MyCurve", curve1, vizparms)
    ob2 = MakeCurve("MyCurve2", curve2, vizparms)
    MakeIpos("Frame", curve1, vizparms)
    MakeIpos("Frame2", curve2, vizparms)
    MakeCameraRotateIpo(vizparms=vizparms)

    if vizparms.planar:
        CurvePlanar(ob1, ob2, vizparms)

    SetDome(ob1, ob2, vizparms)
    SetLayers(vizparms)
    SetAnimParms(vizparms)
    
    #scene = Scene.GetCurrent()
    #scene.update(1)
    #Blender.Window.RedrawAll()

def MakeCurve(name, curve, vizparms):
    ob = bpy.data.objects.get(name)
    me = ob.data
    if me:
        newme = BlenderCurves25.CurveToTube(curve, vizparms=vizparms, me=me)
    else:
        newme = BlenderCurves25.CurveToTube(curve, vizparms=vizparms)
    
    newme.validate()
    newme.update()
    ob.data = newme
    bpy.data.meshes.remove(me)
    
    #ob.link(me)
    #ob.makeDisplayList()

    return ob

def SetDome(ob1, ob2, vizparms):
    dome = bpy.data.objects["Dome"]
    if (vizparms.planar):
        dome.setLocation(0.0, 0.0, 0.0)
        return
    bb1 = ob1.bound_box
    bb2 = ob2.bound_box
    xmin = min(bb1[0][0], bb2[0][0])
    xmax = max(bb1[6][0], bb2[6][0])
    ymin = min(bb1[0][1], bb2[0][1])
    ymax = max(bb1[6][1], bb2[6][1])
    zmin = min(bb1[0][2], bb2[0][2])
    zmax = max(bb1[6][2], bb2[6][2])
    x = (xmin+xmax)/2
    y = (ymin+ymax)/2
    z = (0.73*zmin+0.27*zmax)/2

    dome.location = (x, y, z)
    
def CurvePlanar(ob1, ob2, vizparms):
    scene = bpy.data.scenes["View2D"]
    scene.update(1)

    bb1 = ob1.bound_box
    bb2 = ob2.bound_box
    xmin = int(floor(min(bb1[0][0], bb2[0][0])))
    xmax = int(ceil(max(bb1[7][0], bb2[7][0])))
    ymin = int(floor(min(bb1[0][1], bb2[0][1])))
    ymax = int(ceil(max(bb1[7][1], bb2[7][1])))

    xminpure = int(round(xmin/vizparms.scale))
    xmaxpure = int(round(xmax/vizparms.scale))
    yminpure = int(round(ymin/vizparms.scale))
    ymaxpure = int(round(ymax/vizparms.scale))

    xmin = xminpure * vizparms.scale
    xmax = xmaxpure * vizparms.scale
    ymin = yminpure * vizparms.scale
    ymax = ymaxpure * vizparms.scale


    ob = bpy.data.objects['x_axis']
    ob.location = (xmin-1, 0.0, 0.0)
    ob.scale = (xmax-xmin+2, 1.0, 1.0)

    ob = bpy.data.onjects['x_ticks']
    ob.location = (xmin, 0.0, 0.0)
    arr = ob.modifiers[0]
    arr[Modifier.Settings.COUNT] = max(1, xmaxpure - xminpure + 1)
    arr[Modifier.Settings.OFFSET_VEC] = \
        Mathutils.Vector(vizparms.scale, 0.0, 0.0)
    ob.makeDisplayList()

    ob = bpy.data.objects['x_axis_tip']
    ob.location = (xmax+1, 0.0, 0.0)

    ob = bpy.data.objects['y_axis']
    ob.location = (0.0, ymin-1, 0.0)
    ob.scale = (1.0, ymax-ymin+2, 1.0)

    ob = bpy.data.objects['y_ticks']
    ob.location = (0.0, ymin, 0.0)
    arr = ob.modifiers[0]
    arr.count = max(1, ymaxpure - yminpure + 1)
    arr.constant_offset_displace = \
        Mathutils.Vector(0.0, vizparms.scale, 0.0)
    #ob.makeDisplayList()

    ob = bpy.data.objects['y_axis_tip']
    ob.locaton = (0,ymax+1,0)

    ob = bpy.data.objects['cam2d']
    ob.location = (0.5*(xmax+xmin),0.5*(ymax+ymin),20)
    cam = ob.data
    cam.lens = (2*max(1,max(xmax - xmin + 1, ymax - ymin + 1)))

def MakeIpos(name, curve, vizparms):
    ob = bpy.data.objects[name]
    ob.animation_data_create()
    #ipo = ob.getIpo()
    ob.animation_data.action = BlenderCurves25.CurveToFrenetAction(curve, vizparms=vizparms)
    #if ipo:
        #ipo = BlenderCurves.CurveToFrenetAction(curve, vizparms=vizparms, ipo=ipo)
    #else:
        #ipo = BlenderCurves.CurveToFrenetAction(curve, vizparms=vizparms)
    #ob.setIpo(ipo)

def MakeCameraRotateIpo(vizparms=None):
    if (vizparms == None):
        return
    ob = bpy.data.objects["Camera Rotate"]
    ob.animation_data_create()
    ob.animation_data.action = bpy.data.actions.new(name="MyAction")
    fcu_z = ob.animation_data.action.fcurves.new(data_path="rotation", index=2)
    fcu_z.keyframe_points.add(2)
    fcu_z.keyframe_points[0].co = 1.0, 0.0
    if (vizparms.cameraRotate):
        fcu_z.keyframe_points[1].co = vizparms.endFrame+1.0, -360/10.0
    else:
        fcu_z.keyframe_points[1].co = vizparms.endFrame+1.0, 0.0    
    #ipo = ob.getIpo()
    #if ipo[Ipo.OB_ROTX]:
     #   ipo[Ipo.OB_ROTX] = None
    #if ipo[Ipo.OB_ROTY]:
     #   ipo[Ipo.OB_ROTY] = None
    #if ipo[Ipo.OB_ROTZ]:
    #    ipo[Ipo.OB_ROTZ] = None
    #rotz = ipo.addCurve('RotZ')
    #rotz.append((1.0,0.0))
    #if (vizparms.cameraRotate):
     #   rotz.append((vizparms.endFrame+1.0,-360/10.0))
    #else:
     #   rotz.append((vizparms.endFrame+1.0,0.0))
    #rotz.setInterpolation("Linear")
    #rotz.recalc()

def SetAnimParms(vizparms):
    if (vizparms == None):
        return
    scene = bpy.data.scenes["Composite"]
    scene.frame_end = vizparms.endFrame
    #scene.getRenderingContext().eFrame = vizparms.endFrame
    scene = bpy.data.scenes["Full Frame"]
    #scene.getRenderingContext().eFrame = vizparms.endFrame
    scene = bpy.data.scenes["Composite2D"]
    #scene.getRenderingContext().eFrame = vizparms.endFrame
    scene = bpy.data.scenes["View2D"]
    #scene.getRenderingContext().eFrame = vizparms.endFrame
    scene =bpy.data.scenes["Overlay"]
    #scene.getRenderingContext().eFrame = vizparms.endFrame

    if (vizparms.planar):
        scene = bpy.data.scenes["Composite2D"]
        bpy.context.screen.scene = scene
        #scene.makeCurrent()
    else:
        scene = bpy.data.scenes["Composite"]
        bpy.context.screen.scene = scene
        #scene.makeCurrent()

def SetLayers(vizparms):
    if (vizparms == None):
        return
    scene = bpy.data.scenes["Full Frame"]
    layers = [False]*20
    layers[0] = True
    layers[1] = True
    layers[2] = True
    layers[3] = True    
    if (vizparms.showFrenet):
        layers[10] = True
    if (vizparms.connectFrames):
        layers[11] = True
    if (vizparms.connectZ1):
        layers[12] = True
    if (vizparms.connectZ2):
        layers[13] = True
    scene.layers = layers

    scene = bpy.data.scenes["Overlay"]
    layers = [False]*20
    if (vizparms.planar):
        if (vizparms.showFrameNo):
            layers[13]= True
        if (vizparms.showCurvatureTorsion):
            layers[14]
    else:
        if (vizparms.showFrameNo):
            layers[11]
        if (vizparms.showCurvatureTorsion):
            layers[12]
    scene.layers=layers

    scene = bpy.data.scenes["View2D"]
    layers = [False]*20
    layers[0] = True
    layers[3] = True
    if (vizparms.showFrenet):
        layers[10] = True
    if (vizparms.connectFrames):
        layers[11] = True
    if (vizparms.connectZ1):
        layers[12] = True
    if (vizparms.connectZ2):
        layers[13] = True
    scene.layers = layers
