from math import sqrt, cos, sin, pi
from Vector import *
from Curve import *

class MyCurve1(Curve):
    def __init__(self):
        Curve.__init__(self, self.value, self.deriv1, self.deriv2,
                       self.deriv3, 0, 2*pi,
                       cyclic=False,
                       unitSpeed=False,
                       planar=False)
    def value(self, t):
        x=sin(3*t)
        y=2*sin(4*t)
        z=t**2
        return ([x,y,z])
    def deriv1(self, t):
        x=3*cos(3*t)
        y=8*cos(4*t)
        z=2*t
        return ([x,y,z])
    def deriv2(self, t):
        x=-9*sin(3*t)
        y=-32*sin(4*t)
        z=2
        return ([x,y,z])
    def deriv3(self, t):
        x=-27*cos(3*t)
        y=-128*cos(4*t)
        z=0
        return ([x,y,z])

class MyCurve2(Curve):
    def __init__(self):
        Curve.__init__(self, self.value, self.deriv1, self.deriv2,
                       self.deriv3, 0, 2*pi,
                       cyclic=True,
                       unitSpeed=False,
                       planar=False)
    def value(self, t):
        x=3*sin(3*t)
        y=sin(4*t)
        z=2*cos(2*t)
        return ([x,y,z])
    def deriv1(self, t):
        x=9*cos(3*t)
        y=4*cos(4*t)
        z=-4*sin(2*t)
        return ([x,y,z])
    def deriv2(self, t):
        x=-27*sin(3*t)
        y=-16*sin(4*t)
        z=-8*cos(2*t)
        return ([x,y,z])
    def deriv3(self, t):
        x=-81*cos(3*t)
        y=-64*cos(4*t)
        z=16*sin(2*t)
        return ([x,y,z])

class VizParms:
    def __init__(self):
        self.planar=False
        self.showFrenet=True
        self.connectFrames=True
        self.connectZ1=False
        self.connectZ2=False
        self.showFrameNo=True
        self.showCurvatureTorsion=True
        self.scale=4.0
        self.curveRes=1000
        self.radius=0.05
        self.radRes=4
        self.endFrame=250
        self.timeRes=100
        self.cameraRotate=True
