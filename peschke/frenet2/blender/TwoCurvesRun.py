from math import floor, ceil
#import Blender
#from Blender import Mesh, Object, Material, Mathutils, Modifier, Scene
#from Blender import Ipo, IpoCurve
import bpy, mathutils, imp

import TwoCurves
reload(TwoCurves)
import BlenderCurves
reload(BlenderCurves)

def run():
    curve1 = TwoCurves.MyCurve1()
    curve2 = TwoCurves.MyCurve2()

    try:
        vizparms = TwoCurves.VizParms()
    except:
        vizparms = None

    SetAnimParms(vizparms)
    ob1 = MakeCurve("MyCurve", curve1, vizparms)
    ob2 = MakeCurve("MyCurve2", curve2, vizparms)
    MakeIpos("Frame", curve1, vizparms)
    MakeIpos("Frame2", curve2, vizparms)
    MakeCameraRotateIpo(vizparms=vizparms)

    if vizparms.planar:
        CurvePlanar(ob1, ob2, vizparms)

    SetDome(ob1, ob2, vizparms)
    SetLayers(vizparms)
    SetAnimParms(vizparms)
    scene = Scene.GetCurrent()
    scene.update(1)
    Blender.Window.RedrawAll()

def MakeCurve(name, curve, vizparms):
    ob = Object.Get(name)
    me = ob.getData(mesh=True)
    if me:
        me = BlenderCurves.CurveToTube(curve, vizparms=vizparms, me=me)
    else:
        me = BlenderCurves.CurveToTube(curve, vizparms=vizparms)

    ob.link(me)
    ob.makeDisplayList()

    return ob

def SetDome(ob1, ob2, vizparms):
    dome = Object.Get("Dome")
    if (vizparms.planar):
        dome.setLocation(0.0, 0.0, 0.0)
        return
    bb1 = ob1.getBoundBox(1)
    bb2 = ob2.getBoundBox(1)
    xmin = min(bb1[0][0], bb2[0][0])
    xmax = max(bb1[6][0], bb2[6][0])
    ymin = min(bb1[0][1], bb2[0][1])
    ymax = max(bb1[6][1], bb2[6][1])
    zmin = min(bb1[0][2], bb2[0][2])
    zmax = max(bb1[6][2], bb2[6][2])
    x = (xmin+xmax)/2
    y = (ymin+ymax)/2
    z = (0.73*zmin+0.27*zmax)/2

    dome.setLocation(x, y, z)
    
def CurvePlanar(ob1, ob2, vizparms):
    scene = Scene.Get("View2D")
    scene.update(1)

    bb1 = ob1.getBoundBox(1)
    bb2 = ob2.getBoundBox(1)
    xmin = int(floor(min(bb1[0][0], bb2[0][0])))
    xmax = int(ceil(max(bb1[7][0], bb2[7][0])))
    ymin = int(floor(min(bb1[0][1], bb2[0][1])))
    ymax = int(ceil(max(bb1[7][1], bb2[7][1])))

    xminpure = int(round(xmin/vizparms.scale))
    xmaxpure = int(round(xmax/vizparms.scale))
    yminpure = int(round(ymin/vizparms.scale))
    ymaxpure = int(round(ymax/vizparms.scale))

    xmin = xminpure * vizparms.scale
    xmax = xmaxpure * vizparms.scale
    ymin = yminpure * vizparms.scale
    ymax = ymaxpure * vizparms.scale


    ob = Object.Get('x_axis')
    ob.setLocation(xmin-1, 0.0, 0.0)
    ob.setSize(xmax-xmin+2, 1.0, 1.0)

    ob = Object.Get('x_ticks')
    ob.setLocation(xmin, 0.0, 0.0)
    arr = ob.modifiers[0]
    arr[Modifier.Settings.COUNT] = max(1, xmaxpure - xminpure + 1)
    arr[Modifier.Settings.OFFSET_VEC] = \
        Mathutils.Vector(vizparms.scale, 0.0, 0.0)
    ob.makeDisplayList()

    ob = Object.Get('x_axis_tip')
    ob.setLocation(xmax+1, 0.0, 0.0)

    ob = Object.Get('y_axis')
    ob.setLocation(0.0, ymin-1, 0.0)
    ob.setSize(1.0, ymax-ymin+2, 1.0)

    ob = Object.Get('y_ticks')
    ob.setLocation(0.0, ymin, 0.0)
    arr = ob.modifiers[0]
    arr[Modifier.Settings.COUNT] = max(1, ymaxpure - yminpure + 1)
    arr[Modifier.Settings.OFFSET_VEC] = \
        Mathutils.Vector(0.0, vizparms.scale, 0.0)
    ob.makeDisplayList()

    ob = Object.Get('y_axis_tip')
    ob.setLocation(0,ymax+1,0)

    ob = Object.Get('cam2d')
    ob.setLocation(0.5*(xmax+xmin),0.5*(ymax+ymin),20)
    cam = ob.getData()
    cam.setLens(2*max(1,max(xmax - xmin + 1, ymax - ymin + 1)))

def MakeIpos(name, curve, vizparms):
    ob = Object.Get(name)
    ipo = ob.getIpo()
    if ipo:
        ipo = BlenderCurves.CurveToFrenetIpo(curve, vizparms=vizparms, ipo=ipo)
    else:
        ipo = BlenderCurves.CurveToFrenetIpo(curve, vizparms=vizparms)
    ob.setIpo(ipo)

def MakeCameraRotateIpo(vizparms=None):
    if (vizparms == None):
        return
    ob = Object.Get("Camera Rotate")
    ipo = ob.getIpo()
    if ipo[Ipo.OB_ROTX]:
        ipo[Ipo.OB_ROTX] = None
    if ipo[Ipo.OB_ROTY]:
        ipo[Ipo.OB_ROTY] = None
    if ipo[Ipo.OB_ROTZ]:
        ipo[Ipo.OB_ROTZ] = None
    rotz = ipo.addCurve('RotZ')
    rotz.append((1.0,0.0))
    if (vizparms.cameraRotate):
        rotz.append((vizparms.endFrame+1.0,-360/10.0))
    else:
        rotz.append((vizparms.endFrame+1.0,0.0))
    rotz.setInterpolation("Linear")
    rotz.recalc()

def SetAnimParms(vizparms):
    if (vizparms == None):
        return
    scene = Scene.Get("Composite")
    scene.getRenderingContext().eFrame = vizparms.endFrame
    scene = Scene.Get("Full Frame")
    scene.getRenderingContext().eFrame = vizparms.endFrame
    scene = Scene.Get("Composite2D")
    scene.getRenderingContext().eFrame = vizparms.endFrame
    scene = Scene.Get("View2D")
    scene.getRenderingContext().eFrame = vizparms.endFrame
    scene = Scene.Get("Overlay")
    scene.getRenderingContext().eFrame = vizparms.endFrame

    if (vizparms.planar):
        scene = Scene.Get("Composite2D")
        scene.makeCurrent()
    else:
        scene = Scene.Get("Composite")
        scene.makeCurrent()

def SetLayers(vizparms):
    if (vizparms == None):
        return
    scene = Scene.Get("Full Frame")
    layers = [1,2,3,4]
    if (vizparms.showFrenet):
        layers.append(11)
    if (vizparms.connectFrames):
        layers.append(12)
    if (vizparms.connectZ1):
        layers.append(13)
    if (vizparms.connectZ2):
        layers.append(14)
    scene.setLayers(layers)

    scene = Scene.Get("Overlay")
    layers = []
    if (vizparms.planar):
        if (vizparms.showFrameNo):
            layers.append(14)
        if (vizparms.showCurvatureTorsion):
            layers.append(15)
    else:
        if (vizparms.showFrameNo):
            layers.append(12)
        if (vizparms.showCurvatureTorsion):
            layers.append(13)
    scene.setLayers(layers)

    scene = Scene.Get("View2D")
    layers = [1,4]
    if (vizparms.showFrenet):
        layers.append(11)
    if (vizparms.connectFrames):
        layers.append(12)
    if (vizparms.connectZ1):
        layers.append(13)
    if (vizparms.connectZ2):
        layers.append(14)
    scene.setLayers(layers)
