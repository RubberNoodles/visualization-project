import Blender
from Blender import Object, Scene, Text3d

import TwoCurves
reload(TwoCurves)
import BlenderCurves

def run():
    curve1 = TwoCurves.MyCurve1()
    curve2 = TwoCurves.MyCurve2()
    updateCurvTor(curve1, "curvature", "torsion")
    updateCurvTor(curve2, "curvature2", "torsion2")
    updateFrameNo()
    updateBlender()

def updateCurvTor(curve, curvname, torname):
    start = Blender.Get('staframe')
    end = Blender.Get('endframe')
    cur = Blender.Get('curframe')

    t0 = curve.t0
    t1 = curve.t1

    if (curve.cyclic):
        end = end +1

    t = t0 + (t1-t0) * (cur - start) / (end - start)
    curv = curve.curvature(t)
    tors = curve.torsion(t)

    txt = Text3d.Get(curvname)
    txt.setText("%f" % (curv))
    txt = Text3d.Get(torname)
    txt.setText("%f" % (tors))

def updateFrameNo():
    cur = Blender.Get('curframe')
    txt = Text3d.Get("frameno")
    txt.setText("%d / %d" % (cur, Blender.Get('endframe')))

def updateBlender():
    obname = ["curvature", "curvature2D",
              "curvature2", "curvature2D2",
              "torsion", "torsion2",
              "frameno", "frameno2D"]
    for obn in obname:
        ob = Object.Get(obn)
        ob.makeDisplayList()

    Blender.Window.RedrawAll()
