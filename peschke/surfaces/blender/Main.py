from tkinter import *
import subprocess


class App:

    def __init__(self, master):

        frame = Frame(master)
        frame.pack()

        self.button = Button(
            frame, text="QUIT", fg="red", command=frame.quit
            )
        self.button.pack(side=LEFT)

        self.hi_there = Button(frame, text="Hello", command=self.surface_data)
        self.hi_there.pack(side=LEFT)

    def surface_data(self):
        filepath = "O:/MB/MB/peschke/surfaces/blender/MySurfaceColor.bat"
        p = subprocess.Popen(filepath, shell=True, stdout = subprocess.PIPE)
        stdout, stderr = p.communicate()
        print(p.returncode) # is 0 if success

root = Tk()

app = App(root)

root.mainloop()
root.destroy() # optional; see description below