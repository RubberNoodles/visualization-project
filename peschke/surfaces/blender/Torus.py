from math import sqrt, cos, sin, pi
from Surface import *

class Torus(Surface):
    def __init__(self, rmajor, rminor, unum, vnum):
        Surface.__init__(self, self.formula, \
                         0.0, 2*pi, unum,  \
                         0.0, 2*pi, vnum)
        self.rmajor = rmajor
        self.rminor = rminor

    def formula(self, u, v):
        theta = u
        r = self.rmajor + self.rminor * cos(v)
        z = self.rminor * sin(v)

        x = r * cos(theta)
        y = r * sin(theta)

        return (x,y,z)
