from math import *
#import Blender
#from Blender import Mesh, Object, Material, Mathutils, Ipo
import bpy, sys, imp

# Make sure local modules can be found
#import sys
sys.path.append("")
try:
    import os
    #sys.path.append(os.path.dirname(Blender.Get("filename")))
    #sys.path.append(os.path.dirname(Blender.Get("filename"))+"/../../common")
    sys.path.append(os.path.dirname(bpy.context.blend_data.filepath))
    sys.path.append(os.path.dirname("/../../common"))
    sys.path.append(os.path.dirname(bpy.context.blend_data.filepath)+"/../../common")
    sys.path.append("C:\Python33\Lib\site-packages")
except:
    pass

import MySurfaceColor
imp.reload(MySurfaceColor)
import MySCurveRun25
imp.reload(MySCurveRun25)

import BlenderSurfaces25
from VizParm import VizParms


class DrawCurvePanel(bpy.types.Panel):
    bl_space_type = "VIEW_3D"
    bl_region_type = "TOOLS"
    bl_context = "objectmode"
    bl_label = "Draw Curve"
    
    def draw(self,context):#Enable operators
        TheCol = self.layout.column(align = True);
        
        #Options
        TheCol.prop(context.scene, "uPartition")
        TheCol.prop(context.scene, "vPartition")
        TheCol.prop(context.scene, "show_FrenetX")
        TheCol.prop(context.scene, "show_FrenetY")
        TheCol.prop(context.scene, "show_ParticleX")
        TheCol.prop(context.scene, "show_ParticleY")
        
        TheCol.operator("mesh.draw_curve", text = "Draw Surface")



class DrawSurface(bpy.types.Operator):#Main Draw curve method
    bl_idname = "mesh.draw_curve"
    bl_label = "Draw Curve"
    bl_options ={"UNDO"}
    def invoke(self, context, event):
        mat=bpy.data.materials.new(name='UV')
        surface = MySurfaceColor.MySurface()
        bpy.ops.object.select_all(action='DESELECT')
        u = 0
        v = 0
        while True:
            try:
                bpy.data.objects["testx"+str(u)].select = True
                bpy.data.objects["testx"+str(u)+'Frame'].select = True
                bpy.data.objects["testx"+str(u)+'Probe'].select = True
            #	bpy.data.objects["testx"+str(u)+'Probe2D'].select = True
                u+=1
            except:
                break
        while True:
            try:
                bpy.data.objects["testy"+str(v)].select = True
                bpy.data.objects["testy"+str(v)+'Frame'].select = True
                bpy.data.objects["testy"+str(v)+'Probe'].select = True
                #bpy.data.objects["testy"+str(v)+'Probe2D'].select = True
                v+=1
            except:
                break	
        bpy.ops.object.delete()
        #scale = float(context.scene.scale)
        if context.scene.show_FrenetX:
            context.scene.show_ParticleX = False
        if context.scene.show_FrenetY:
            context.scene.show_ParticleY = False
        #Set visual parameter that are to be shown
        #visparm.Set\
            #(
                #context.scene.show_Frenet,
                #context.scene.show_Inset,
                #context.scene.show_Osculating,
                #context.scene.show_Rectifying, 
                #context.scene.show_NormalPlane,
                #context.scene.show_FrameNo, 
                #context.scene.show_CurTor,
                #context.scene.show_Particle,
                #float(context.scene.scale),
                #int(context.scene.curveRes),
                #float(context.scene.radius),
                #int(context.scene.radRes),
                #int(context.scene.endFrame),
                #int(context.scene.timeRes),
                #int(context.scene.cameraDelay)
            #)
        #print(context.scene.show_Particle)
        
        

        try:
            ob = bpy.data.objects[surface.return_name()+"Surface"]
            me = ob.data
            if me:
                 me = BlenderSurfaces25.SurfaceToMesh(surface, surface.resu, surface.resv, me=me)
            else:
                 me = BlenderSurfaces25.SurfaceToMesh(surface, surface.resu, surface.resv)
                 ob.link(me)
            #ob.makeDisplayList()
            ob.data=me
        except Exception as e:
            me = BlenderSurfaces25.SurfaceToMesh(surface, surface.resu, surface.resv)
            ob = bpy.data.objects.new(surface.return_name()+"Surface",me)
            scn = bpy.context.collection
            scn.objects.link(ob)

        #Blender.Window.RedrawAll()
        vizparmx = VizParms()
        vizparmy = VizParms()
        vizparmx.Set(context.scene.show_FrenetX,False,False,False,False,False,False,context.scene.show_ParticleX,6.0,1000,0.05, 4,1000)
        vizparmy.Set(context.scene.show_FrenetY,False,False,False,False,False,False,context.scene.show_ParticleY,6.0,1000,0.05, 4,1000)
    
        #MySCurveRun25.run("test",int(context.scene.uPartition),int(context.scene.vPartition),vizparmx,vizparmy) 
        #visparm.SshowInset(context.scene.show_Inset)
        #visparm.SshowCurvatureTorsion(context.scene.show_CurTor)
        #MyCurveRun25.run(context.scene.ob_name, visparm) #Running main program
        #for text in surface.texts:
        #	if text != "":
        #		bpy.ops.object.text_add(location = (0,0,0))
        #		ob=bpy.context.object
        #		ob.data.body = text
        #		ob.name = text
        #		ob.scale = (3.0,3.0,3.0)
        
        # Fix up some nodes!
        mat = bpy.data.materials['UV']
        mat.use_nodes = True
        #idk what this does
        nodes = mat.node_tree.nodes
        node_col=nodes.new(type="ShaderNodeAttribute")
        node_col.attribute_name = "color"
        node_col.location=-200,250

        node_BSDF=nodes.get('Principled BSDF')
        links = mat.node_tree.links
        links.new(node_col.outputs[0], node_BSDF.inputs[0])
        return {"FINISHED"}
    
def register():
    bpy.utils.register_class(DrawSurface)
    bpy.utils.register_class(DrawCurvePanel)
    
    #Optionn

    bpy.types.Scene.uPartition = bpy.props.StringProperty \
        (
            name = "Number of Partition in x",
            description = "Set how many partition in variable x",
            default = "10"
        )
    bpy.types.Scene.vPartition = bpy.props.StringProperty \
        (
            name = "Number of Partition in y",
            description = "Set how many partition in variable y",
            default = "10"
        )
    bpy.types.Scene.show_FrenetX = bpy.props.BoolProperty\
        (
            name = "Show Frenet Serret Frame for X Coordinateline",
            description = "Show the particle and the Frenet-Serret Frame (This has priority display than Particle)",
            default = True
        )
    bpy.types.Scene.show_FrenetY = bpy.props.BoolProperty\
        (
            name = "Show Frenet Serret Frame for Y Coordinateline",
            description = "Show the particle and the Frenet-Serret Frame (This has priority display than Particle)",
            default = True
        )
    bpy.types.Scene.show_ParticleX = bpy.props.BoolProperty\
        (
            name = "Show Particle for X Coordinateline",
            description = "Show the particle and the Frenet-Serret Frame (This has priority display than Particle)",
            default = True
        )
    bpy.types.Scene.show_ParticleY = bpy.props.BoolProperty\
        (
            name = "Show Particle for Y Coordinateline",
            description = "Show the particle and the Frenet-Serret Frame (This has priority display than Particle)",
            default = True
        )

register()
