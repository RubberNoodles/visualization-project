from math import *
from Vector import *
from Surface import * #Draw Surface
from Curve import * #Draw Coordinate Lines

class MySurface(Surface):
    def __init__(self):
        Surface.__init__(self, self.value, self.du, self.dv,
                       0, 2*pi, 0, pi,
                       uCyclic=False, vCyclic=False)
    def value(self, u, v):
        x=(1.0+.1*cos(8*v)+.1*(cos(2*v)-1.0)*sin(8*u))*cos(u)*sin(v)
        y=(1.0+.1*cos(8*v)+.1*(cos(2*v)-1.0)*sin(8*u))*sin(u)*sin(v)
        z=(1.0+.1*cos(8*v)+.1*(cos(2*v)-1.0)*sin(8*u))*cos(v)
        return (ScalMult(6.0,[x,y,z]))
    def du(self, u, v):
        x=.8*(cos(2*v)-1.0)*cos(8*u)*cos(u)*sin(v)-(1.0+.1*cos(8*v)+.1*(cos(2*v)-1.0)*sin(8*u))*sin(u)*sin(v)
        y=.8*(cos(2*v)-1.0)*cos(8*u)*sin(u)*sin(v)+(1.0+.1*cos(8*v)+.1*(cos(2*v)-1.0)*sin(8*u))*cos(u)*sin(v)
        z=.8*(cos(2*v)-1.0)*cos(8*u)*cos(v)
        return (ScalMult(6.0,[x,y,z]))
    def dv(self, u, v):
        x=(-.8*sin(8*v)-.2*sin(2*v)*sin(8*u))*cos(u)*sin(v)+(1.0+.1*cos(8*v)+.1*(cos(2*v)-1.0)*sin(8*u))*cos(u)*cos(v)
        y=(-.8*sin(8*v)-.2*sin(2*v)*sin(8*u))*sin(u)*sin(v)+(1.0+.1*cos(8*v)+.1*(cos(2*v)-1.0)*sin(8*u))*sin(u)*cos(v)
        z=(-.8*sin(8*v)-.2*sin(2*v)*sin(8*u))*cos(v)-(1.0+.1*cos(8*v)+.1*(cos(2*v)-1.0)*sin(8*u))*sin(v)
        return (ScalMult(6.0,[x,y,z]))

class CoordinateX(Curve):
