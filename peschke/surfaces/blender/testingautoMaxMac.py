import subprocess


while True:
    try:
        with open("params.mac",'w') as f:
            x = input("What is the equation for x? ")
            y = input("What is the equation for y? ")
            z = input("What is the equation for z? ")
            f.write(x +'\n'+y+'\n'+z)
        path = '/opt/local/bin:/opt/local/sbin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin'
        #One issue is that I can't automatically find where the maxima command resides within the shell.
        subprocess.run("maxima -b MySurfaceColorFile.mac",
            shell=True, env={'PATH' : path}) #Ok this failsafe doesn't work
        break
    except:
        print("Your variables aren't compatible with maxima, try again")

print("Creating render ...")
bfile='MySurfaceTest.blend'
blender_path='/Applications/Blender.app/Contents/MacOS/Blender'
subprocess.run(f"{blender_path} {bfile} -b --python testingBlendAuto.py",
               shell=True)
print("Render completed")


## FYI it will be important to change my directory later
# https://stackoverflow.com/questions/21406887/subprocess-changing-directory
