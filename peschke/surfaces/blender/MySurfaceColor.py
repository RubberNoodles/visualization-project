from Vector import *
from Surface import *
from Curve import *
#from PIL import Image
import os, sys, bpy, importlib
import csv

#im = Image.open("O:\\MB\\MB\peschke\\surfaces\\blender\\img\\world.200411.3x5400x2700.jpg")
#sys.path.append(os.path.dirname(bpy.context.blend_data.filepath+"/../../blender")+'/surf_data')
#im = Image.open(os.path.realpath(bpy.context.blend_data.filepath+"/../../blender")+'/Dissidia_012_World.jpg')
#pix = im.load()

def reshape(file_path):
    with open(file_path) as f:
        spamreader = csv.reader(f, delimiter=',')
        lists=[]
        list_dict={}
        uSteps=0
        vSteps=0
        vList=[] #The output array we want should be uSteps rows of vSteps triples
        for ind,line in enumerate(spamreader):
            if ind == 0:
                # The first line of the csv is a list of all the parameters
                for val in line:
                    if not val.isnumeric():
                        lists.append(val)
                    else:
                        if uSteps == 0:
                            uSteps=int(val)
                        else:
                            vSteps=int(val)
                    arrlen=uSteps*vSteps #The dimensions of each array
                    list_dict['uSteps']=uSteps
                    list_dict['vSteps']=vSteps
                    for list_data in lists:
                        list_dict[list_data] = []
            else:
                # Reshaping the lists
                line = [float(i) for i in line]
                if ind % vSteps == 0:
                    vList.append(line)
                    list_dict[lists[(ind-1)//arrlen]].append(vList)
                    vList=[]
                else:
                    vList.append(line)
        return list_dict

class MySurface(Surface):
    def __init__(self, data):
        Surface.__init__(self, self.value, self.du, self.dv,
                       0.0, 6.283185307179586, -1.570796326794896, 1.570796326794896,
                       uCyclic=False, vCyclic=True,
                       red=self.red, red0=0.8, red1=0.9,
                       green=self.green, green0=1.2, green1=1.6,
                       blue=self.blue, blue0=1.0, blue1=1.2)
        data=reshape(data)
        self.name="MySurface"
        self.resu=data['uSteps']
        self.resv=data['vSteps']     
        self.valueList=data['valueList']
        self.uDeriv1List=data['uDeriv1List']
        self.vDeriv1List=data['vDeriv1List']
    
    def return_name(self):
        return self.name
    def value(self, u, v):
        (u,v) = (int(u),int(v))
        return (ScalMult(1.0,self.valueList[u][v]))
    def du(self, u, v):
        return (ScalMult(1.0,self.uDeriv1List[u][v]))
    def dv(self, u, v):
        return (ScalMult(1.0,self.vDeriv1List[u][v]))
    def cube_root(self, value):
        if value< 0:
            b=-(-value)**(1/3)
        else:
          b = (value)**(1/3)
        return b


    def red(self, u, v):
        return 0.5
    def green(self, u, v):

        return 0.5
    def blue(self, u, v):

        return 0.5

