import bpy, mathutils, imp
from math import floor, ceil, pi
from VizParm import VizParms
import MySurfaceColor
imp.reload(MySurfaceColor)
import BlenderCurves25
imp.reload(BlenderCurves25)

def run(name = 'test', x = 1, y = 1, vizparmsx = VizParms(), vizparmsy = VizParms()):
    #print(vizparms.showParticle)
    #print(MySurfaceColor.MySuface.u0)
    curve = MySurfaceColor.YCoordinateLine()
    if y!= 0:
        step = (curve.t1 - curve.t0)/y


        #print(step)
    #try:
        #vizparms = Ivizparms
    #except:
        #vizparms = None

        SetAnimParms(curve, vizparmsy)
        for n in range(y):
            curve.setCounter(curve.t0+n*step)
            MakeCurve(curve, vizparmsy, name+'y'+str(n))
            MakeActions(curve, vizparmsy, name+'y'+str(n))

    curve = MySurfaceColor.XCoordinateLine()

    if x!= 0:

        step = (curve.t1 - curve.t0)/x
        #print(step)
    #try:
        #vizparms = Ivizparms
    #except:
        #vizparms = None

    #SetAnimParms(curve, vizparms)
        SetAnimParms(curve, vizparmsx)
        for n in range(x):
            curve.setCounter(n*step)
            MakeCurve(curve, vizparmsx, name+'x'+str(n))
            MakeActions(curve, vizparmsx, name+'x'+str(n))
    #SetLayers(vizparms)
    #CameraTrack(vizparms)

    # Replace? q
    #scene = Scene.GetCurrent()
    #scene.update(1)
    #Blender.Window.RedrawAll()

def MakeCurve(curve, vizparms, name = "MyCurve"):

    try:#Already exist
        ob = bpy.data.objects[name]
        #print(ob)
        me = ob.data

        if me != None:
           newme = BlenderCurves25.CurveToTube(curve, name, vizparms=vizparms, me=me)
        else:
             newme = BlenderCurves25.CurveToTube(curve, name, vizparms=vizparms)


        newme.validate()
        newme.update()
        ob.data = newme
        bpy.data.meshes.remove(me)

    except Exception as e:#Create new
           #print(e)
           me = BlenderCurves25.CurveToTube(curve, vizparms=vizparms)
           ob=bpy.data.objects.new(name,me)
           scn = bpy.context.scene
           scn.objects.link(ob)
           #scn.objects.active = ob
           #ob.select = True



    #newme = BlenderCurves25.CurveToTube(curve, vizparms=vizparms,me)

    #newme.validate()
    #newme.update()
    #ob.data = newme
    #bpy.data.meshes.remove(me)

    if curve.planar:
        CurvePlanar(ob, vizparms)

def CurvePlanar(curveob, vizparms):
    bb = curveob.bound_box
    xmin = int(floor(bb[0][0]))
    xmax = int(ceil(bb[7][0]))
    ymin = int(floor(bb[0][1]))
    ymax = int(ceil(bb[7][1]))


    if vizparms.scale != 0:
        xminpure = int(round(xmin/vizparms.scale))
        xmaxpure = int(round(xmax/vizparms.scale))
        yminpure = int(round(ymin/vizparms.scale))
        ymaxpure = int(round(ymax/vizparms.scale))


        xmin = xminpure * vizparms.scale
        xmax = xmaxpure * vizparms.scale
        ymin = yminpure * vizparms.scale
        ymax = ymaxpure * vizparms.scale


    ob = bpy.data.objects["x_axis"]
    ob.location = (-10.0, 0.0, 0.0)
    ob.scale = (20.0, 1.0, 1.0)

    ob = bpy.data.objects["x_ticks"]
    ob.location = (-9, 0.0, 0.0)
    arr = ob.modifiers[0]
    arr.count = max(1, 7)
    arr.constant_offset_displace = (vizparms.scale, 0.0, 0.0)
    #ob.makeDisplayList()

    ob = bpy.data.objects["x_axis_tip"]
    ob.location = (10, 0.0, 0.0)

    ob = bpy.data.objects["y_axis"]
    ob.location = (0.0, -8.0, 0.0)
    ob.scale = (1.0, 16.0, 1.0)

    ob = bpy.data.objects["y_ticks"]
    ob.location = (0.0, -9, 0.0)
    arr = ob.modifiers[0]
    arr.count = max(1, 7)
    arr.constant_offset_displace = (0.0, vizparms.scale, 0.0)
    #ob.makeDisplayList()

    ob = bpy.data.objects["y_axis_tip"]
    ob.location = (0,8,0)

    ob = bpy.data.objects["cam2d"]
    ob.location = (0.0,0.0,20)
    cam = ob.data
    cam.lens = 2*max(1,max(xmax - xmin + 1, ymax - ymin + 1))

def MakeActions(curve,vizparms, name = "MyCurve"):
    #print(vizparms.showParticle)
    if vizparms.showParticle:
        me = bpy.data.meshes["particle"]
    elif vizparms.showFrenet:
        me = bpy.data.meshes["FSProbe"]

    try:
        ob = bpy.data.objects[name +"Frame"]
        probe = bpy.data.objects[name+"Probe"]
        probe.data = me
        if vizparms.showParticle:
            probe.scale=((0.25,0.25,0.25))
        else:
            probe.scale=((0.5,0.5,0.5))            
    except:
        #frame_source = bpy.data.objects["Frame"]
        #probe_source = bpy.data.objects["FSProbe"]
        bpy.ops.object.add(type='EMPTY')
        bpy.context.active_object.name = name + "Frame"
        ob = bpy.data.objects[name+"Frame"]
        newprobe = bpy.data.objects.new(name+"Probe", me)
        if vizparms.showParticle:
            newprobe.scale=((0.25,0.25,0.25))
        else:
            newprobe.scale=((0.5,0.5,0.5))
        #probe2D=bpy.data.objects.new(name+"Probe2D",bpy.data.meshes["Probe2D"])
        #newprobe.data = probe_source.data.copy()

        #bpy.context.scene.objects.link(ob)
        newprobe.layers[10] = True
        newprobe.layers[19] = True
        bpy.context.scene.objects.link(newprobe)
        #bpy.context.scene.objects.link(probe2D)
        newprobe.parent = ob
        #probe2D.parent = ob


        #ob.scale = frame_source.scale
        #ob.location = frame_source.location
        newprobe.rotation_euler = [0,0,-pi/2]
        #probe2D.scale = [0.3,0.3,0.3]
        #probe2D.rotation_euler

    ob.animation_data_create()
    ob.animation_data.action = \
        BlenderCurves25.CurveToFrenetAction(curve, vizparms=vizparms)

    #ob = bpy.data.objects["SuperFrame"]
   # ob.animation_data_create()
    #ob.animation_data.action = \
        #BlenderCurves25.CurveToFrenetAction(curve, vizparms=vizparms)

    #try:
    #    ob = bpy.data.objects[name + "Particle"]
    #except:
    #       ob = bpy.data.objects.new(name+"Particle", bpy.data.meshes["particle"])
    #       ob.scale = ((0.25,0.25,0.25))
    #       ob.layers[4] = True
    #       scn = bpy.context.scene
    #       scn.objects.link(ob)
    #ob.animation_data_create()
    #ob.animation_data.action = \
    #    BlenderCurves25.CurveToFrenetAction(curve, vizparms=vizparms)


    #try:
        #probe2D = bpy.data.objects[name + "Probe2D"]
    #except:


    #ob = bpy.data.objects["CameraFrame"]
    #ob.animation_data_create()
    #ob.animation_data.action = \
    #    BlenderCurves25.CurveToFrenetAction(curve, vizparms=vizparms,
    #                                      offset=vizparms.cameraDelay)

    #ob = bpy.data.objects["CameraTrack"]
    #ob.animation_data_create()
    #ob.animation_data.action = \
    #    BlenderCurves25.CurveToFrenetAction(curve, vizparms=vizparms,
    #                                        Loc=True, Rot=False)

    #MakeCameraRotateAction(vizparms=vizparms)

def MakeCameraRotateAction(vizparms=None):
    if (vizparms == None):
        return
    ob = bpy.data.objects["Camera Rotate"]
    ob.animation_data_create()
    ob.animation_data.action = bpy.data.actions.new(name="MyAction")
    fcu_z = ob.animation_data.action.fcurves.new(data_path="rotation", index=2)
    fcu_z.keyframe_points.add(2)
    fcu_z.keyframe_points[0].co = 1.0, 0.0
    if (vizparms.cameraRotate):
        fcu_z.keyframe_points[1].co = vizparms.endFrame+1.0, -360/10.0
    else:
        fcu_z.keyframe_points[1].co = vizparms.endFrame+1.0, 0.0

def SetAnimParms(curve, vizparms):
    if (vizparms == None):
        return
    scene = bpy.data.scenes["Scene"]
    scene.frame_end = vizparms.endFrame
    #scene = bpy.data.scenes["Full Frame"]
    #scene.frame_end = vizparms.endFrame
    #scene = bpy.data.scenes["Inset"]
    #scene.frame_end = vizparms.endFrame
    #scene = bpy.data.scenes["UVs"]
    #scene.frame_end = vizparms.endFrame

    #scene = bpy.data.scenes["View2D"]
    #scene.frame_end = vizparms.endFrame
    #if (curve.planar):
    #    bpy.context.screen.scene = scene
    #else:
    #    if (vizparms.showFrenet and vizparms.showInset):
    #        scene = bpy.data.scenes["Composite"]
    #        bpy.context.screen.scene = scene
    #    else:
    #        scene = bpy.data.scenes["Full Frame"]
    #        bpy.context.screen.scene = scene

def SetLayers(vizparms):
    if (vizparms == None):
        return
    scene = bpy.data.scenes["Full Frame"]

    layers = [False]*20
    layers[0] = True
    layers[1] = True
    layers[2] = True
    layers[3] = True
    layers[11] = True

    if (vizparms.showFrenet):
        layers[10] = True
        if (vizparms.showFrameNo):
            layers[11] = True
        if (vizparms.showCurvatureTorsion):
            layers[12] = True
        if (vizparms.showInset):
            layers[13] = True
    if (vizparms.showParticle):
       layers[4] = True

    scene.layers = layers

    scene = bpy.data.scenes["View2D"]
    layers = [False]*20
    layers[0] = True
    layers[3] = True
    if (vizparms.showFrenet):
        layers[10] = True
        if (vizparms.showFrameNo):
            layers[11] = True
        if (vizparms.showCurvatureTorsion):
            layers[12] = True
    scene.layers = layers

    scene = bpy.data.scenes["Inset"]
    layers = [False]*20
    layers[0] = True
    layers[1] = True
    layers[2] = True
    layers[3] = True
    if (vizparms.showOsculating):
        layers[10] = True
    if (vizparms.showRectifying):
        layers[11] = True
    if (vizparms.showNormalPlane):
        layers[12] = True
    scene.layers = layers

def CameraTrack(vizparms):
    ob = bpy.data.objects["Camera Frame"]
    const = ob.constraints[0]
    if (vizparms.binormalUp):
        const.up_axis = 'UP_Y'
    else:
        const.up_axis = 'UP_X'
