import subprocess


while True:
    try:
        with open("params.mac",'w') as f:
        #defaults to sphere
            x = input("What is the equation for x? ") or "x(u,v):=sin(u)*cos(v);"
            y = input("What is the equation for y? ") or "y(u,v):=sin(u)*sin(v);"
            z = input("What is the equation for z? ") or "z(u,v):=cos(u);"
            
            f.write(x +'\n'+y+'\n'+z)
        #path = '/opt/local/bin:/opt/local/sbin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin'
        #One issue is that I can't automatically find where the maxima command resides within the shell.
        #subprocess.run("maxima -b MySurfaceColorFile.mac",
        #    shell=True, env={'PATH' : path}) #Ok this failsafe doesn't work
        subprocess.run("maxima -b MySurfaceColorFile.mac", shell=True)
        break
    except:
        print("Your variables aren't compatible with maxima, try again")

print("Creating render ...")
bfile='MySurfaceTest.blend'
#I need to try finding the the location of blender (i.e. it might not be on PATH)
subprocess.run(f"blender {bfile} -b --python testingBlendAuto.py",
               shell=True)
print("Render completed")


## FYI it will be important to change my directory later
# https://stackoverflow.com/questions/21406887/subprocess-changing-directory
