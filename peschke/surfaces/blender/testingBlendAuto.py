from math import *
#import Blender
#from Blender import Mesh, Object, Material, Mathutils, Ipo
import bpy, sys, importlib

# Make sure local modules can be found
#import sys
sys.path.append("")
try:
    import os
    #sys.path.append(os.path.dirname(Blender.Get("filename")))
    #sys.path.append(os.path.dirname(Blender.Get("filename"))+"/../../common")
    sys.path.append(os.path.dirname(bpy.context.blend_data.filepath))
    sys.path.append(os.path.dirname("/../../common"))
    sys.path.append(os.path.dirname(bpy.context.blend_data.filepath)+"/../../common")
except:
    pass

import MySurfaceColor
importlib.reload(MySurfaceColor)

import BlenderSurfaces25
importlib.reload(BlenderSurfaces25)
from VizParm import VizParms

bpy.data.scenes[0].render.filepath = os.path.dirname(bpy.context.blend_data.filepath)+'/test.png'
bpy.context.scene.render.image_settings.file_format = 'PNG'

#----
# Constant temporarily
file_path= os.path.dirname(bpy.context.blend_data.filepath)+ '/surf_data/surf0.txt'
surface = MySurfaceColor.MySurface(file_path)

##if bpy.context.scene.show_FrenetX:
##    context.scene.show_ParticleX = False
##if bpy.context.scene.show_FrenetY:
##    context.scene.show_ParticleY = False
##
try:
    ob = bpy.data.objects[surface.return_name()+"Surface"]
    me = ob.data
    if me:
         me = BlenderSurfaces25.SurfaceToMesh(surface, surface.resu, surface.resv, me=me)
    else:
         me = BlenderSurfaces25.SurfaceToMesh(surface, surface.resu, surface.resv)
         ob.link(me)
    ob.data=me
except Exception as e:
    me = BlenderSurfaces25.SurfaceToMesh(surface, surface.resu, surface.resv)
    ob = bpy.data.objects.new(surface.return_name()+"Surface",me)
    scn = bpy.context.collection
    scn.objects.link(ob)

bpy.context.scene.frame_set(0)
bpy.ops.render.render(write_still=True)
           


