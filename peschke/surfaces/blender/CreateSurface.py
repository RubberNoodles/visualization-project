import Blender
from Blender import Mesh, Object, Material, Mathutils, Ipo
import Torus
import BlenderSurfaces

torus = Torus.Torus(2,1,32,40)

ob = Object.Get('Torus')
me = ob.getData(mesh=True)
if me:
    me = BlenderSurfaces.SurfaceToMesh(torus, me=me)
else:
    me = BlenderSurfaces.SurfaceToMesh(torus)
    ob.link(me)
ob.makeDisplayList()

Blender.Window.RedrawAll()