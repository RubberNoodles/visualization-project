from math import floor, ceil
import Blender
from Blender import Mesh, Object, Material, Mathutils, Ipo, Modifier

cameradelay = -5

# Make sure local modules can be found
import sys
sys.path.append("")
try:
    import os
    sys.path.append(os.path.dirname(Blender.Get("filename")))
    sys.path.append(os.path.dirname(Blender.Get("filename"))+"/../../common")
except:
    pass

#print sys.path
import MyCurve
reload(MyCurve)
import BlenderCurves

curve = MyCurve.MyCurve()

ob = Object.Get("MyCurve")
me = ob.getData(mesh=True)
if me:
    me = BlenderCurves.CurveToTube(curve, 1000, 4, 0.05, me=me)
else:
    me = BlenderCurves.CurveToTube(curve, 1000, 4, 0.05)
    ob.link(me)
ob.makeDisplayList()
bb = ob.getBoundBox(1)
xmin = int(floor(bb[0][0]))
xmax = int(ceil(bb[7][0]))
ymin = int(floor(bb[0][1]))
ymax = int(ceil(bb[7][1]))

ob = Object.Get("Frame")
ipo = ob.getIpo()
if ipo:
    ipo = BlenderCurves.CurveToFrenetIpo(curve, 100, ipo=ipo)
else:
    ipo = BlenderCurves.CurveToFrenetIpo(curve, 100)
    ob.setIpo(ipo)

ob = Object.Get("CameraFrame")
ipo = ob.getIpo()
if ipo:
    ipo = BlenderCurves.CurveToFrenetIpo(curve, 100, ipo=ipo, offset=cameradelay)
else:
    ipo = BlenderCurves.CurveToFrenetIpo(curve, 100, offset=cameradelay)
    ob.setIpo(ipo)

Blender.Window.RedrawAll()

ob = Object.Get("CameraTrack")
ipo = ob.getIpo()
if ipo:
    ipo = BlenderCurves.CurveToFrenetIpo(curve, 100, ipo=ipo, offset=cameradelay, Loc=False, Rot=True)
else:
    ipo = BlenderCurves.CurveToFrenetIpo(curve, 100, offset=cameradelay, Loc=False, Rot=True)
    ob.setIpo(ipo)
ipo = BlenderCurves.CurveToFrenetIpo(curve, 100, ipo=ipo, Loc=True, Rot=False)

if curve.planar:
    ob = Object.Get('x_axis')
    ob.setLocation(xmin,0,0)
    arr = ob.modifiers[0]
    arr[Modifier.Settings.COUNT] = max(1, xmax - xmin + 1)
    ob.makeDisplayList()

    ob = Object.Get('x_axis_tip')
    ob.setLocation(xmax+1,0,0)

    ob = Object.Get('y_axis')
    ob.setLocation(0,ymin,0)
    arr = ob.modifiers[0]
    arr[Modifier.Settings.COUNT] = max(1, ymax - ymin + 1)
    ob.makeDisplayList()

    ob = Object.Get('y_axis_tip')
    ob.setLocation(0,ymax+1,0)

    ob = Object.Get('cam2d')
    ob.setLocation(0.5*(xmax+xmin),0.5*(ymax+ymin),20)

Blender.Window.RedrawAll()