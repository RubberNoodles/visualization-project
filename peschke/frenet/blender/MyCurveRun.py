from math import floor, ceil
import Blender
from Blender import Mesh, Object, Material, Mathutils, Modifier, Scene
from Blender import Ipo, IpoCurve, Constraint

import MyCurve
reload(MyCurve)
import BlenderCurves
reload(BlenderCurves)

def run():
    curve = MyCurve.MyCurve()

    try:
        vizparms = MyCurve.VizParms()
    except:
        vizparms = None

    SetAnimParms(curve, vizparms)
    MakeCurve(curve, vizparms)
    MakeIpos(curve, vizparms)
    SetLayers(vizparms)
    CameraTrack(vizparms)
    scene = Scene.GetCurrent()
    scene.update(1)
    Blender.Window.RedrawAll()

def MakeCurve(curve, vizparms):
    ob = Object.Get("MyCurve")
    me = ob.getData(mesh=True)
    if me:
        me = BlenderCurves.CurveToTube(curve, vizparms=vizparms, me=me)
    else:
        me = BlenderCurves.CurveToTube(curve, vizparms=vizparms)

    ob.link(me)
    ob.makeDisplayList()

    if curve.planar:
        CurvePlanar(ob, vizparms)

def CurvePlanar(curveob, vizparms):
    bb = curveob.getBoundBox(1)
    xmin = int(floor(bb[0][0]))
    xmax = int(ceil(bb[7][0]))
    ymin = int(floor(bb[0][1]))
    ymax = int(ceil(bb[7][1]))

    xminpure = int(round(xmin/vizparms.scale))
    xmaxpure = int(round(xmax/vizparms.scale))
    yminpure = int(round(ymin/vizparms.scale))
    ymaxpure = int(round(ymax/vizparms.scale))

    xmin = xminpure * vizparms.scale
    xmax = xmaxpure * vizparms.scale
    ymin = yminpure * vizparms.scale
    ymax = ymaxpure * vizparms.scale


    ob = Object.Get('x_axis')
    ob.setLocation(xmin-1, 0.0, 0.0)
    ob.setSize(xmax-xmin+2, 1.0, 1.0)

    ob = Object.Get('x_ticks')
    ob.setLocation(xmin, 0.0, 0.0)
    arr = ob.modifiers[0]
    arr[Modifier.Settings.COUNT] = max(1, xmaxpure - xminpure + 1)
    arr[Modifier.Settings.OFFSET_VEC] = \
        Mathutils.Vector(vizparms.scale, 0.0, 0.0)
    ob.makeDisplayList()

    ob = Object.Get('x_axis_tip')
    ob.setLocation(xmax+1, 0.0, 0.0)

    ob = Object.Get('y_axis')
    ob.setLocation(0.0, ymin-1, 0.0)
    ob.setSize(1.0, ymax-ymin+2, 1.0)

    ob = Object.Get('y_ticks')
    ob.setLocation(0.0, ymin, 0.0)
    arr = ob.modifiers[0]
    arr[Modifier.Settings.COUNT] = max(1, ymaxpure - yminpure + 1)
    arr[Modifier.Settings.OFFSET_VEC] = \
        Mathutils.Vector(0.0, vizparms.scale, 0.0)
    ob.makeDisplayList()

    ob = Object.Get('y_axis_tip')
    ob.setLocation(0,ymax+1,0)

    ob = Object.Get('cam2d')
    ob.setLocation(0.5*(xmax+xmin),0.5*(ymax+ymin),20)
    cam = ob.getData()
    cam.setLens(2*max(1,max(xmax - xmin + 1, ymax - ymin + 1)))

def MakeIpos(curve, vizparms):
    ob = Object.Get("Frame")
    ipo = ob.getIpo()
    if ipo:
        ipo = BlenderCurves.CurveToFrenetIpo(curve, vizparms=vizparms, ipo=ipo)
    else:
        ipo = BlenderCurves.CurveToFrenetIpo(curve, vizparms=vizparms)
    ob.setIpo(ipo)

    ob = Object.Get("CameraFrame")
    ipo = ob.getIpo()
    if ipo:
        ipo = BlenderCurves.CurveToFrenetIpo(curve, vizparms=vizparms,
                                             ipo=ipo,
                                             offset=vizparms.cameraDelay)
    else:
        ipo = BlenderCurves.CurveToFrenetIpo(curve, vizparms=vizparms,
                                             offset=vizparms.cameraDelay)
    ob.setIpo(ipo)

    ob = Object.Get("CameraTrack")
    ipo = ob.getIpo()
    if ipo:
        ipo = BlenderCurves.CurveToFrenetIpo(curve, vizparms=vizparms,
                                             ipo=ipo,
                                             offset=vizparms.cameraDelay,
                                             Loc=False, Rot=True)
    else:
        ipo = BlenderCurves.CurveToFrenetIpo(curve, vizparms=vizparms,
                                             offset=vizparms.cameraDelay,
                                             Loc=False, Rot=True)
    ob.setIpo(ipo)
    ipo = BlenderCurves.CurveToFrenetIpo(curve, vizparms=vizparms,
                                         ipo=ipo, Loc=True, Rot=False)

    MakeCameraRotateIpo(vizparms=vizparms)

def MakeCameraRotateIpo(vizparms=None):
    if (vizparms == None):
        return
    ob = Object.Get("Camera Rotate")
    ipo = ob.getIpo()
    if ipo[Ipo.OB_ROTX]:
        ipo[Ipo.OB_ROTX] = None
    if ipo[Ipo.OB_ROTY]:
        ipo[Ipo.OB_ROTY] = None
    if ipo[Ipo.OB_ROTZ]:
        ipo[Ipo.OB_ROTZ] = None
    rotz = ipo.addCurve('RotZ')
    rotz.append((1.0,0.0))
    if (vizparms.cameraRotate):
        rotz.append((vizparms.endFrame+1.0,-360/10.0))
    else:
        rotz.append((vizparms.endFrame+1.0,0.0))
    rotz.setInterpolation("Linear")
    rotz.recalc()

def SetAnimParms(curve, vizparms):
    if (vizparms == None):
        return
    scene = Scene.Get("Composite")
    scene.getRenderingContext().eFrame = vizparms.endFrame
    scene = Scene.Get("Full Frame")
    scene.getRenderingContext().eFrame = vizparms.endFrame
    scene = Scene.Get("Inset")
    scene.getRenderingContext().eFrame = vizparms.endFrame
    scene = Scene.Get("UVs")
    scene.getRenderingContext().eFrame = vizparms.endFrame

    scene = Scene.Get("View2D")
    scene.getRenderingContext().eFrame = vizparms.endFrame
    if (curve.planar):
        scene.makeCurrent()
    else:
        if (vizparms.showFrenet and vizparms.showInset):
            scene = Scene.Get("Composite")
            scene.makeCurrent()
        else:
            scene = Scene.Get("Full Frame")
            scene.makeCurrent()

def SetLayers(vizparms):
    if (vizparms == None):
        return
    scene = Scene.Get("Full Frame")
    layers = [1,2,3,4]
    if (vizparms.showFrenet):
        layers.append(11)
        if (vizparms.showFrameNo):
            layers.append(12)
        if (vizparms.showCurvatureTorsion):
            layers.append(13)
        if (vizparms.showInset):
            layers.append(14)
    scene.setLayers(layers)

    scene = Scene.Get("View2D")
    layers = [1,4]
    if (vizparms.showFrenet):
        layers.append(11)
        if (vizparms.showFrameNo):
            layers.append(12)
        if (vizparms.showCurvatureTorsion):
            layers.append(13)
    scene.setLayers(layers)

    scene = Scene.Get("Inset")
    layers = [1,2,3,4]
    if (vizparms.showOsculating):
        layers.append(11)
    if (vizparms.showRectifying):
        layers.append(12)
    if (vizparms.showNormalPlane):
        layers.append(13)
    scene.setLayers(layers)
    
def CameraTrack(vizparms):
    ob = Object.Get('Camera Frame')
    const = ob.constraints[0]
    if (vizparms.binormalUp):
        offs = const[Constraint.Settings.UP] = Constraint.Settings.UPY
    else:
        offs = const[Constraint.Settings.UP] = Constraint.Settings.UPX
