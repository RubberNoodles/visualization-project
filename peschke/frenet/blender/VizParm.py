class VizParms():
    def __init__(self): #constructor
        self.showFrenet=True
        self.showInset=True
        self.showOsculating=True
        self.showRectifying=False
        self.showNormalPlane=False
        self.showFrameNo=True
        self.showCurvatureTorsion=True
        self.showParticle = True
        self.scale=1.0
        self.curveRes=1000 #number of steps
        self.radius=0.05
        self.radRes=100
        self.endFrame=1000
        self.timeRes=100
        self.cameraDelay=-5
        self.cameraRotate=False
        self.binormalUp=True

    def Set \
        (
            self,
            showFrenet=True,
            showInset=True,
            showOsculating=False,
            showRectifying=False,
            showNormalPlane=False,
            showFrameNo=False,
            showCurvatureTorsion=True,
            showParticle=True,
            scale=1.0,
            curveRes=1000, #set curve resolution/steps
            radius=0.05,
            radRes=100,
            endFrame=1000,
            timeRes=100,
            cameraDelay=-5,
            cameraRotate=False,
            binormalUp=True
        ):
        self.showFrenet=showFrenet
        self.showInset=showInset
        self.showOsculating=showOsculating
        self.showRectifying=showRectifying
        self.showNormalPlane=showNormalPlane
        self.showFrameNo=showFrameNo
        self.showCurvatureTorsion=showCurvatureTorsion
        self.showParticle = showParticle
        self.scale=scale
        self.curveRes=curveRes
        self.radius = radius
        self.radRes=radRes
        self.endFrame=endFrame
        self.timeRes=timeRes
        self.cameraDelay=cameraDelay
        self.cameraRotate=cameraRotate
        self.binormalUp=binormalUp



#a = VizParms()
#a.SshowFrenet(False)
#print(a.showFrenet)

#print("done")