from math import sqrt, cos, sin, pi
from Vector import *
from Curve import *

class MyCurve(Curve):
    def __init__(self):
        Curve.__init__(self, self.value, self.deriv1, self.deriv2,
                       self.deriv3, 0, 2*pi,
                       cyclic=True,
                       unitSpeed=False,
                       planar=False)
    def value(self, t):
        x=sin(3*t)
        y=sin(4*t)
        z=2*cos(2*t)
        return ([x,y,z])
    def deriv1(self, t):
        x=3*cos(3*t)
        y=4*cos(4*t)
        z=-4*sin(2*t)
        return ([x,y,z])
    def deriv2(self, t):
        x=-9*sin(3*t)
        y=-16*sin(4*t)
        z=-8*cos(2*t)
        return ([x,y,z])
    def deriv3(self, t):
        x=-27*cos(3*t)
        y=-64*cos(4*t)
        z=16*sin(2*t)
        return (ScalMult(6.0,[x,y,z]))

class VizParms:
    def __init__(self):
        self.showFrenet=True
        self.showInset=True
        self.showOsculating=False
        self.showRectifying=False
        self.showNormalPlane=False
        self.showFrameNo=True
        self.showCurvatureTorsion=True
        self.scale=6.0
        self.curveRes=1000
        self.radius=0.05
        self.radRes=4
        self.endFrame=250
        self.timeRes=100
        self.cameraDelay=-5
        self.cameraRotate=False
        self.binormalUp=False
