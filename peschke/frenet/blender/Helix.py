from math import sqrt, cos, sin, pi
from Curve import *

class Helix(Curve):
    def __init__(self, a, b, t0, t1):
        Curve.__init__(self, self.value, self.deriv1, self.deriv2,
                       self.deriv3, t0, t1, unitSpeed = True)
        self.a = a
        self.b = b
        self.c = 1.0 / sqrt(a*a + b*b)
    def value(self, s):
        a = self.a
        b = self.b
        k = self.c
        T = k*s
        return (a*cos(T), a*sin(T), b*T)
    def deriv1(self, s):
        a = self.a
        b = self.b
        k = self.c
        AK = a*k
        T = k*s
        return (-AK*sin(T), AK*cos(T), b*k)
    def deriv2(self, s):
        a = self.a
        b = self.b
        k = self.c
        AKK = a*k*k
        T = k*s
        return (-AKK*cos(T), -AKK*sin(T), 0)
    def deriv3(self, s):
        a = self.a
        b = self.b
        k = self.c
        T = k*s
        AKKK = a*k*k*k
        return (AKKK*sin(T), -AKKK*cos(T), 0)
