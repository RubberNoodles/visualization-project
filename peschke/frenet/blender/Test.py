from math import sqrt, cos, sin, pi
from Vector import *
from Curve import *


class Test(Curve):
    def __init__(self):
        self.t0 = 0
        self.t1 = 2*pi
    def value(self, s):
        x = 2*sin(s) + cos(s)
        y = 2*sin(s) - 2*cos(s)
        z = sin(s) + 2*cos(s)
        return (ScalMult(1.0/3,[x,y,z]))
    def deriv1a(self, s):
        x = 2*cos(s) - sin(s)
        y = 2*cos(s) + 2*sin(s)
        z = cos(s) - 2*sin(s)
        return (ScalMult(1.0/3,[x,y,z]))
    def deriv2a(self, s):
        x = -2*sin(s) + cos(s)
        y = -2*sin(s) + 2*cos(s)
        z = -sin(s) - 2*cos(s)
        return (ScalMult(1.0/3,[x,y,z]))
    def deriv3a(self, s):
        x = -2*cos(s) - sin(s)
        y = -2*cos(s) - 2*sin(s)
        z = -cos(s) + 2*sin(s)
        return (ScalMult(1.0/3,[x,y,z]))
